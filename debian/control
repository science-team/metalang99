Source: metalang99
Section: devel
Priority: optional
Maintainer: Roland Mas <lolando@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 doxygen,
Standards-Version: 4.6.2
Homepage: https://github.com/Hirrolot/metalang99
Vcs-Browser: https://salsa.debian.org/science-team/metalang99
Vcs-Git: https://salsa.debian.org/science-team/metalang99.git

Package: libmetalang99-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libjs-mathjax,
Description: Full-blown preprocessor metaprogramming
 Metalang99 is a firm foundation for writing reliable and maintainable
 metaprograms in pure C99. It is implemented as an interpreted FP
 language atop of preprocessor macros: just #include <metalang99.h>
 and you are ready to go. Metalang99 features algebraic data types,
 pattern matching, recursion, currying, and collections; in addition,
 it provides means for compile-time error reporting and
 debugging. With our built-in syntax checker, macro errors should be
 perfectly comprehensible, enabling you for convenient development.
